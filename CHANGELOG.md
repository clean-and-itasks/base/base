# Changelog

## v3.0.0

- Feature: add specifying types for expressions with syntax: `(expr :: type)`,
  uniqueness attribute equalities or type contexts are not allowed. The
  specified type may be more general, uniqueness attributes that are required
  because of propagation are not added by the compiler, so for example `[*Int]`
  should be `*[*Int]`.
- Change: only import generic cases/derives that are imported by an explicit
  import or import of the whole module, instead of importing all generic
  cases/derives in all (partially) imported definition modules.
- Enhancement: several enhancements/bug fixes due to updating to `base-compiler`
  v4.

### v2.1.0

- Feature: add support for `{ :}` array types.
- Enhancement: several enhancements due to updating to `base-compiler` v3.

## v2.0

- Change: include `base-lib` package which has many name conflicts with
  `clean-platform` and cannot be used together.

#### v1.0.1

- Enhancement: reduce memory usage of unboxed arrays of basic types.

## v1.0

First tagged version.
