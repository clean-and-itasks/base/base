# base

This is the repository of `base`, a metapackage containing the bare minimum to
create [Clean][] applications.

It contains:

- [`base-clm`](https://gitlab.com/clean-and-itasks/base/clm)
- [`base-code-generator`](https://gitlab.com/clean-and-itasks/base/code-generator)
- [`base-compiler`](https://gitlab.com/clean-and-itasks/base/compiler)
- [`base-lib`](https://gitlab.com/clean-and-itasks/base/lib)
- [`base-linker`](https://gitlab.com/clean-and-itasks/base/linker) (Windows only)
- [`base-rts`](https://gitlab.com/clean-and-itasks/base/rts)
- [`base-stdenv`](https://gitlab.com/clean-and-itasks/base/stdenv)

## Versioning in base

These are some notes on how `base` and its subpackages are versioned.

`base` uses semantic versioning from the Clean programmer's perspective. This
is somewhat tricky for a metapackage:

- A major update indicates backwards incompatible changes. This typically means
  that code that previously worked may not work any more, i.e., that there is a
  backwards incompatible change in syntax.
- The patch version is updated when the dependencies must be updated but do not
  contain user-facing backwards incompatible changes.
- `base` has dependencies with `^` constraints (the first non-zero version part
  must remain the same). Concretely this means that if `base` 1.1.0 depends on
  `base-compiler: ^2.0`, the same version of `base` may use different versions
  of the compiler, which may have different features (if they were added in
  different minor versions of `base-compiler`).
- For this reason, there are no minor updates of `base` for most new features.
  If you depend on certain new features, add an extra dependency to your
  project, e.g. `base: ^1.0` and `base-compiler: >=2.2`. We do not have a real
  use for minor updates of `base` itself.
- When a new feature is backwards compatible but significantly restricts the
  allowed dependencies, a minor update of `base` can be useful. This was done
  for 2.1, which added `{ :}` array support. This feature required the latest
  versions of various `base-*` packages. Using a minor release allows future
  changes to be backported onto 2.0.x if needed.

The underlying packages use semantic versioning from the build tool's
perspective. For instance, the `base-compiler` major version number is updated
when ABC code changes in a backwards-incompatible manner (e.g., added or
changed instructions, but not removed instructions). The minor version would be
updated for new syntax; the patch version for bug fixes and performance
improvements.

### Example: adding a new ABC instruction

- The code generator needs a minor update to support the new instruction. This
  is minor because it adds functionality in a backwards-compatible manner; all
  previously valid ABC code will remain valid.
- The compiler needs a major update to add the instruction. This is major
  because it changes the (implicit) ABC specification in a
  backwards-incompatible manner; projects like the ABC interpreter depend on
  this.
- `base` itself needs a patch update to allow the new compiler version. This
  version should also specify the earliest code generator version which can be
  used with the new compiler. For `base`, this is just a patch update (and not
  minor/major), because from the programmer's perspective nothing has changed.

## Maintainer

This project is maintained by [Camil Staps][].

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
